<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('products')->delete();
        
        \DB::table('products')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Ommavorö',
                'slug' => 'ommavoro',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam pulvinar, neque eu ullamcorper ullamcorper, risus elit accumsan ante, id sollicitudin est leo nec sem.',
                'image' => 'DWUlQ2s9DLNa6dNVwr1Fwf5iYzdOarErG4385rsF.jpg',
                'price' => 16.99,
                'created_at' => '2021-01-04 12:14:38',
                'updated_at' => '2021-01-06 12:19:50',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Dalock',
                'slug' => 'dalock',
                'description' => 'Aliquam ornare sollicitudin metus at blandit. Pellentesque quis nibh sollicitudin, feugiat tortor ac, euismod metus. Curabitur luctus eu neque eget luctus.',
                'image' => 'IgYpV1RtMRydgeEaqn0NciOo5CJ50n00VenwpIDD.jpg',
                'price' => 1190.0,
                'created_at' => '2021-01-04 12:17:46',
                'updated_at' => '2021-01-06 12:19:50',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Fassanvola',
                'slug' => 'fassanvola',
                'description' => 'Pellentesque egestas interdum feugiat. Mauris hendrerit, enim sit amet posuere elementum, nisi nunc posuere nibh, id sodales sem justo at ipsum. Sed bibendum est eget odio scelerisque vulputate. Vivamus at diam non arcu tempor porttitor. Ut sed libero sed urna porttitor fermentum eget ut arcu.',
                'image' => 'KvZgvHVJUwthRhJ2tmeIAlsuHJhmNttOQ8M7tJua.jpg',
                'price' => 1497.99,
                'created_at' => '2021-01-04 12:19:24',
                'updated_at' => '2021-01-06 12:19:50',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Rekmarta',
                'slug' => 'rekmarta',
                'description' => 'Phasellus sollicitudin tempor diam vel ultricies. Donec imperdiet, purus sodales pretium maximus, magna ligula convallis nunc, eget suscipit dui enim ut leo. Vivamus tincidunt ultrices urna, id scelerisque nunc lacinia nec. Aliquam tincidunt, orci et commodo lacinia, nibh metus tempor mauris, in luctus enim nibh eget nisi. Aliquam vestibulum, magna a finibus posuere, risus est rhoncus mauris, eleifend facilisis massa neque quis mi.',
                'image' => 'GMjbwx9sUs86tmUjkBA0rpxiwehRdJqtXzoYREUg.jpg',
                'price' => 741.45,
                'created_at' => '2021-01-04 12:20:36',
                'updated_at' => '2021-01-06 12:19:50',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Bart',
                'slug' => 'bart',
                'description' => 'Donec erat est, ultricies eu odio in, imperdiet aliquet massa. Vivamus orci odio, ultricies a justo nec, bibendum rutrum ex. Nulla facilisi. Duis cursus iaculis sapien dictum laoreet.',
                'image' => 'piFZqBwtG1YkilywJRqlKB04TVtP5QGuSJOnWJBl.jpg',
                'price' => 9.99,
                'created_at' => '2021-01-04 12:21:02',
                'updated_at' => '2021-01-06 12:19:50',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Flätenind',
                'slug' => 'flatenind',
                'description' => 'Nunc a tincidunt diam. Sed nulla ipsum, venenatis quis urna ac, vulputate mollis ante. Donec condimentum fringilla lectus, quis ultricies erat ultricies at. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Donec suscipit nisi risus, id hendrerit eros mattis id. Sed vel egestas tellus. Ut suscipit vitae libero ac placerat. Suspendisse potenti. Nulla sed purus odio. Suspendisse pulvinar velit purus.',
                'image' => 'oT9zEWDmJscsL9Pw4N2omXsdSzfZW2ENPITgGFqh.jpg',
                'price' => 5.99,
                'created_at' => '2021-01-04 12:21:27',
                'updated_at' => '2021-01-06 12:19:51',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Märablip',
                'slug' => 'marablip',
                'description' => 'Mauris viverra neque tellus, et ultricies magna eleifend id. Curabitur tincidunt id velit ac pharetra. Praesent in sem pretium, bibendum nunc eget, aliquet lorem. Curabitur nec risus risus. Curabitur lacinia urna nibh, in accumsan mauris tristique et. Maecenas rutrum, libero a consequat molestie, ligula mi ullamcorper turpis, vitae pellentesque neque mauris pharetra neque. Sed vulputate quam tortor, sit amet commodo turpis fermentum nec.',
                'image' => 'ebecHYzWZEk9iZsr2mhm5tPHmgIZycqaa5J4LkcK.jpg',
                'price' => 816.3,
                'created_at' => '2021-01-04 12:22:08',
                'updated_at' => '2021-01-06 12:19:51',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Mörn',
                'slug' => 'morn',
                'description' => 'Etiam eu maximus diam. Vestibulum congue mauris eget sollicitudin rutrum. In hac habitasse platea dictumst. Interdum et malesuada fames ac ante ipsum primis in faucibus. Proin pretium hendrerit eleifend. Donec rutrum, odio eu consectetur malesuada, nisi mi egestas purus, et lacinia lectus libero ac dui. Integer purus elit, ultrices id nisi at, congue egestas leo. Aliquam et orci nulla.',
                'image' => 'YQPapa170tOb0dgaXo6n2Xvhitg3FD1ikJd8bqkS.jpg',
                'price' => 122.85,
                'created_at' => '2021-01-04 12:22:37',
                'updated_at' => '2021-01-06 12:19:51',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Brodden',
                'slug' => 'brodden',
                'description' => 'Tortor condimentum lacinia quis vel. Nunc eget lorem dolor sed viverra. Nulla aliquet porttitor lacus luctus accumsan tortor posuere ac ut. Duis at consectetur lorem donec massa sapien.',
                'image' => 'm57eEvYwuxy7bTna7CzIGY1TmgBPd9k4nxw10Q8A.jpg',
                'price' => 172.34,
                'created_at' => '2021-01-05 09:09:31',
                'updated_at' => '2021-01-06 12:19:51',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Norp',
                'slug' => 'norp',
                'description' => 'Non odio euismod lacinia at quis risus. Amet risus nullam eget felis eget nunc lobortis mattis. Eget magna fermentum iaculis eu. Libero nunc consequat interdum varius sit amet mattis vulputate. Risus sed vulputate odio ut.',
                'image' => 'Gq4S9aYaZmgka5w2bAweBZxLuUBIlzuBCITMVgsj.jpg',
                'price' => 87.24,
                'created_at' => '2021-01-05 09:10:03',
                'updated_at' => '2021-01-06 12:19:51',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Flån',
                'slug' => 'flan',
                'description' => 'Nunc sed augue lacus viverra vitae congue eu consequat. Elit eget gravida cum sociis natoque penatibus et magnis dis. Non nisi est sit amet facilisis magna etiam. Ornare arcu odio ut sem nulla pharetra diam sit.',
                'image' => 'InXQGCMK568HaibahKLtz4Q7RvM7vhmIIEdsHAHZ.jpg',
                'price' => 52.45,
                'created_at' => '2021-01-05 09:10:34',
                'updated_at' => '2021-01-06 12:19:51',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Almåbodmotosjö',
                'slug' => 'almabodmotosjo',
                'description' => 'Lectus mauris ultrices eros in cursus turpis massa. Feugiat pretium nibh ipsum consequat nisl vel. Nibh praesent tristique magna sit amet. Auctor urna nunc id cursus. Habitasse platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper dignissim. Malesuada fames ac turpis egestas sed tempus urna et pharetra.',
                'image' => 'luEBlutK2FBlMvPH9CiM4hXvAyIXFPgiy5XpxNNc.jpg',
                'price' => 9912.5,
                'created_at' => '2021-01-05 09:11:10',
                'updated_at' => '2021-01-06 12:19:51',
            ),
        ));
        
        
    }
}