<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class NovaSettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('nova_settings')->delete();

        \DB::table('nova_settings')->insert(array (
            0 =>
            array (
                'key' => 'order',
                'value' => 'desc',
            ),
            1 =>
            array (
                'key' => 'perPage',
                'value' => '8',
            ),
        ));


    }
}
