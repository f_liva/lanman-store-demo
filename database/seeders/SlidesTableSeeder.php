<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SlidesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('slides')->delete();
        
        \DB::table('slides')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Agenda cartonata marrone e blu',
                'image' => 'wCBaFDWufY9RO42TLvOvf9QJt3FcIlxCrGY18Cs0.jpg',
                'bgPosition' => 'bottom',
                'created_at' => '2021-01-05 15:18:28',
                'updated_at' => '2021-01-05 16:10:51',
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Orologio in vero bamboo',
                'image' => 'JPxEpPz3kTWPuvywb1eu17sEF8yFb6fli8PtWEHk.jpg',
                'bgPosition' => 'right',
                'created_at' => '2021-01-05 15:19:00',
                'updated_at' => '2021-01-05 16:11:11',
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Set di cuscini e piumone con fantasia',
                'image' => 'I7naiwSZUr9K8VBd7ULYkVK22ZWBcjbdRBnUl12Q.jpg',
                'bgPosition' => 'right',
                'created_at' => '2021-01-05 15:19:24',
                'updated_at' => '2021-01-05 16:10:34',
            ),
        ));
        
        
    }
}