// require('./bootstrap');

const moveTo = new MoveTo()

document.querySelectorAll('a[href^="#"]')
        .forEach((anchor) => moveTo.registerTrigger(anchor))

const swiper = new Swiper('.swiper-container', {
    loop: true,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
})
