<div @unless($random) x-data="{spotlight: false}" @keydown.escape="spotlight = false" @endif>
    <div class="container mx-auto flex items-center flex-wrap pt-4 pb-12">
        <nav id="store" class="w-full z-30 top-0 px-6 py-1">
            <div class="w-full container mx-auto flex flex-wrap items-center justify-between mt-0 px-2 py-3">
                <h2 class="tracking-wide no-underline hover:no-underline font-bold text-gray-800 text-xl">{{ $title }}</h2>

                {{-- Filters --}}
                @unless($random)
                    <div class="flex items-center space-x-3" id="store-nav-content">
                        <a class="inline-block no-underline hover:text-black cursor-pointer"
                           wire:click.prevent="reverseOrder">
                            @svg($order, 'fill-current hover:text-black')
                        </a>
                        <a class="inline-block no-underline hover:text-black cursor-pointer"
                           @click.prevent="spotlight = true; $nextTick(() => $refs.input.focus())">
                            @svg('search', 'fill-current hover:text-black')
                        </a>
                        @if($query)
                            <button class="border px-3 flex items-center hover:border-black focus:outline-none"
                                    wire:click="resetQuery">{{ $query }} @svg('cancel', 'h-2 ml-1')
                            </button>
                        @endif
                    </div>
                @endif
            </div>
        </nav>

        {{-- Products --}}
        @foreach($products as $product)
            <livewire:product :product="$product" :key="time() . $product->id"/>
        @endforeach
    </div>

    @unless($random)
        {{-- Pagination --}}
        <div class="container mx-auto px-6">
            {{ $products->links() }}
        </div>

        {{-- Spotlight --}}
        <div class="fixed left-0 top-0 w-full h-full bg-white bg-opacity-75 z-30" x-cloak
             x-show="spotlight">
            <label class="fixed left-0 right-0 top-1/2 w-full transform -translate-y-1/2 px-5 md:px-0">
                <input type="search" id="spotlight" placeholder="{{ __('Cerca un prodotto...') }}"
                       wire:model="query" x-ref="input" @click.away="spotlight = false"
                       @keydown.enter="spotlight = false"
                       class="block w-full md:w-2/3 lg:w-1/2 mx-auto appearance-none shadow-lg border outline-none p-5 px-7 text-xl"/>
            </label>
        </div>
    @endif
</div>
