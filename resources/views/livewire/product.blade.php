<div class="w-full md:w-1/3 xl:w-1/4 p-6 flex flex-col">
    <a href="{{ route('product', $product->slug) }}">
        <img class="hover:grow hover:shadow-lg" src="{{ asset("storage/$product->image") }}" alt="{{ $product->name }}">
        <div class="pt-3 flex items-center justify-between">
            <p class="">{{ $product->name }}</p>
            <a class="cursor-pointer" wire:click="toggleLike({{ $product->id }})">
                @if($product->wasLiked())
                    @svg('heart', 'h-6 w-6 fill-current text-red-500 hover:text-red-700')
                @else
                    @svg('heart', 'h-6 w-6 fill-current text-gray-500 hover:text-black')
                @endif
            </a>
        </div>
        <p class="pt-1 text-gray-900">{{ $product->price }}€</p>
    </a>
</div>
