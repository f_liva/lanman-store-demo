<div class="relative mx-auto max-w-hero">
    <div class="swiper-container h-hero">
        <div class="swiper-wrapper">

            @foreach($slides as $slide)
                <div class="swiper-slide">
                    <div
                        class="block h-full w-full mx-auto flex pt-6 md:pt-0 md:items-center bg-cover bg-{{ $slide->bgPosition ?: 'center' }}"
                        style="background-image: url({{ asset('storage/' . $slide->image) }});">

                        <div class="container mx-auto">
                            <div
                                class="flex flex-col w-full lg:w-1/2 md:ml-16 items-center md:items-start px-6 tracking-wide">
                                <p class="text-black text-2xl my-4">{{ $slide->title }}</p>
                                <a class="text-xl inline-block no-underline border-b border-gray-600 leading-relaxed hover:text-black hover:border-black"
                                   href="#catalogue">vedi il prodotto</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
        <div class="swiper-pagination"></div>
        <div
            class="swiper-button-prev w-10 h-10 ml-2 md:ml-10 absolute cursor-pointer text-3xl font-bold text-black hover:text-white rounded-full bg-white hover:bg-gray-900 leading-tight text-center z-10 inset-y-0 left-0 my-auto">
            ‹
        </div>
        <div
            class="swiper-button-next w-10 h-10 mr-2 md:mr-10 absolute cursor-pointer text-3xl font-bold text-black hover:text-white rounded-full bg-white hover:bg-gray-900 leading-tight text-center z-10 inset-y-0 right-0 my-auto">
            ›
        </div>
    </div>
</div>
