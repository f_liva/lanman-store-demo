<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>
        @isset($title)
            {{ $title }} | {{ config('app.name') }}
        @else
            {{ config('app.name') }}
        @endif
    </title>
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:200,400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@6.4.5/swiper-bundle.min.css">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    @livewireStyles
</head>
<body class="bg-white text-gray-600 leading-normal text-base tracking-normal">

    {{-- Navigation --}}
    <nav id="header" class="w-full z-30 top-0 py-1">
        <div class="w-full container mx-auto flex flex-wrap items-center justify-between mt-0 px-6 py-3">
            <label for="menu-toggle" class="cursor-pointer md:hidden block">
                @svg('menu', 'fill-current text-gray-900')
            </label>
            <input class="hidden" type="checkbox" id="menu-toggle"/>

            {{-- Menu --}}
            <div class="hidden md:flex md:items-center md:w-auto w-full order-3 md:order-1" id="menu">
                <nav>
                    <ul class="md:flex items-center justify-between text-base text-gray-700 pt-4 md:pt-0">
                        <li>
                            <a class="inline-block no-underline hover:text-black hover:underline py-2 px-4"
                               href="#catalogue">Catalogo</a>
                        </li>
                        <li>
                            <a class="inline-block no-underline hover:text-black hover:underline py-2 px-4"
                               href="#about">Chi siamo</a>
                        </li>
                    </ul>
                </nav>
            </div>

            {{-- Logo --}}
            <div class="order-1 md:order-2 md:absolute md:left-1/2 md:transform md:-translate-x-1/2">
                <a class="flex items-center tracking-wide no-underline hover:no-underline font-bold text-gray-800 text-xl "
                   href="{{ route('home') }}">@svg('bag', 'fill-current text-gray-800 mr-2') LANMAN</a>
            </div>

            {{-- Utilities --}}
            <div class="order-2 md:order-3 flex items-center" id="nav-content">
                <a class="inline-block no-underline hover:text-black" href="{{ config('nova.path') }}">
                    @svg('user', 'fill-current hover:text-black')
                </a>
                {{--                <a class="pl-3 inline-block no-underline hover:text-black" href="#">--}}
                {{--                    @svg('cart', 'fill-current hover:text-black')--}}
                {{--                </a>--}}
            </div>
        </div>
    </nav>

    {{-- Page Content --}}
    @yield('body')

    {{-- Footer --}}
    <footer class="container mx-auto bg-white py-8 border-t border-gray-400">
        <div class="container flex px-3 py-8 ">
            <div class="w-full mx-auto flex flex-wrap">
                <div class="flex w-full lg:w-1/2 ">
                    <div class="px-3 md:px-0">
                        <h3 class="font-bold text-gray-900">Contatti</h3>
                        <p class="py-4">
                            Tincidunt augue interdum velit euismod in pellentesque. Consectetur adipiscing elit ut
                            aliquam purus sit amet. Ut tortor pretium viverra suspendisse potenti.
                        </p>
                    </div>
                </div>
                <div class="flex w-full lg:w-1/2 lg:justify-end lg:text-right">
                    <div class="px-3 md:px-0">
                        <h3 class="font-bold text-gray-900">Social</h3>
                        <ul class="list-reset flex space-x-3 pt-3">
                            <li>
                                <a class="inline-block no-underline hover:text-black hover:underline py-1"
                                   href="https://it-it.facebook.com/lanmangroup" target="_blank">Facebook</a>
                            </li>
                            <li>
                                <a class="inline-block no-underline hover:text-black hover:underline py-1"
                                   href="https://www.instagram.com/lanman_group/" target="_blank">Instagram</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.8.0/dist/alpine.min.js" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/moveto@1.8.2/dist/moveTo.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/swiper@6.4.5/swiper-bundle.min.js"></script>
    <script src="{{ mix('js/app.js') }}"></script>

    @livewireScripts
</body>
</html>
