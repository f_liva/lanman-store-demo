<p class="mt-8 text-center text-xs text-80">
    &copy; {{ date('Y') }} {{ config('app.name') }} - A minimalist admin panel built with ❤ and Laravel Nova.
</p>
