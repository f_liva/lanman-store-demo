@extends('partials.master', ['title' => $product->name])

@section('body')
    <div class="bg-gray-200">
        <div class="container mx-auto p-8">
            <div class="mb-8 text-sm">
                <a href="{{ route('home') }}" class="hover:text-black hover:underline">Catalogo</a> /
                <a class="text-gray-500">{{ $product->name }}</a>
            </div>
            <div class="md:flex mt-16 md:mb-16">
                <div class="md:w-1/2 md:pr-8 lg:pr-12">
                    <img src="{{ asset("storage/$product->image") }}" alt="{{ $product->name }}" class="mx-auto">
                </div>
                <div class="md:w-1/2 md:pl-8 lg:pl-12">
                    <h1 class="text-3xl md:text-4xl mt-16 md:mt-0">{{ $product->name }}</h1>
                    <p class="text-xl mt-2">{{ $product->price }}€</p>
                    <p class="mt-8 mb-12">{{ $product->description }}</p>
                    <!-- AddToAny BEGIN -->
                    <a class="a2a_dd" href="https://www.addtoany.com/share">
                        @svg('share', 'h-7 w-7 fill-current text-gray-500 hover:text-black')
                    </a>
                    <script async src="https://static.addtoany.com/menu/page.js"></script>
                    <!-- AddToAny END -->
                </div>
            </div>
        </div>
    </div>

    <div class="container mx-auto">
        <div class="px-8 md:w-3/4 lg:w-2/3 mx-auto my-8">
            <nav class="w-full text-center text-lg mb-16">
                <a href="" class="border-b pb-1">Descrizione</a>
            </nav>
            <p>{{ $product->description }}</p>
        </div>
    </div>

    <section class="mt-12 md:my-24">
        <livewire:products title="ALTRI PRODOTTI" :random="4"/>
    </section>
@endsection
