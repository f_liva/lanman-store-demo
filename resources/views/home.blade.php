@extends('partials.master')

@section('body')
    {{-- Hero --}}
    <x-carousel/>

    {{-- Catalogue --}}
    <section id="catalogue" class="bg-white py-8">
        <livewire:products/>
    </section>

    {{-- About --}}
    <section id="about" class="bg-white py-8">
        <div class="container py-8 px-6 mx-auto">
            <h2 class="tracking-wide no-underline hover:no-underline font-bold text-gray-800 text-xl mb-8">CHI
                SIAMO</h2>
            <p class="mt-8 mb-8">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Lectus nulla at volutpat diam ut venenatis. Varius sit amet mattis vulputate enim
                nulla aliquet. Orci sagittis eu volutpat odio. Pulvinar mattis nunc sed blandit libero. Velit laoreet id
                donec ultrices tincidunt arcu. Massa enim nec dui nunc mattis enim ut. Dolor sit amet consectetur
                adipiscing elit pellentesque habitant morbi. Nullam ac tortor vitae purus faucibus. A iaculis at erat
                pellentesque. Nisl vel pretium lectus quam id leo in vitae. Venenatis cras sed felis eget velit aliquet
                sagittis id. Justo eget magna fermentum iaculis. Pharetra vel turpis nunc eget lorem dolor. Mauris
                commodo quis imperdiet massa tincidunt nunc.
            </p>
        </div>
    </section>
@endsection
