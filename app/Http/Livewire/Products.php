<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;

class Products extends Component
{
    use WithPagination;

    /**
     * @var \App\Models\Product[]|\Illuminate\Database\Eloquent\Collection|mixed
     */
    protected $products;

    /**
     * @var string
     */
    public string $order;

    /**
     * @var int
     */
    public int $perPage;

    /**
     * @var string
     */
    public string $query;

    /**
     * @var string
     */
    public string $title = 'CATALOGO';

    /**
     * @var int|null
     */
    public ?int $random = null;

    /**
     * Initialize some things before the rendering.
     */
    public function mount()
    {
        $this->order = nova_get_setting('order', 'asc');
        $this->perPage = nova_get_setting('perPage', 8);
        $this->query = session('query', ''); // Retrieve the query present in the session or initialize it.
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        $this->products = $this->getProducts();

        return view('livewire.products', ['products' => $this->products]);
    }

    /**
     * Reverse the order of the displayed products.
     */
    public function reverseOrder()
    {
        $this->resetPage(); // Reset the pagination.

        $this->order = $this->order == 'asc' ? 'desc' : 'asc';
    }

    /**
     * Reset the search query.
     */
    public function resetQuery()
    {
        $this->updated('query', $this->query = ''); // Trigger the updated method to reset query also in the session.
    }

    /**
     * @param $name
     * @param $value
     */
    public function updated($name, $value)
    {
        if ($name == 'query') {
            session([$name => $value]); // Store the query in the user's session.
        }
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    protected function getProducts()
    {
        if ($this->random) {
            return \App\Models\Product::inRandomOrder()->limit($this->random)->get();
        }

        $products = \App\Models\Product::orderBy('id', $this->order);

        if ($this->query) {
            $products = $products->search($this->query);
        }

        return $products->paginate($this->perPage);
    }
}
