<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Product extends Component
{
    /**
     * @var \App\Models\Product|mixed
     */
    public $product;

    /**
     * @param \App\Models\Product $product
     */
    public function mount(\App\Models\Product $product)
    {
        $this->product = $product;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function render()
    {
        return view('livewire.product', ['product' => $this->product]);
    }

    /**
     * Add or remove the product from favorites.
     */
    public function toggleLike()
    {
        $this->product->toggleLike();
    }
}
