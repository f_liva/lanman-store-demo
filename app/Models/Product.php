<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Nicolaslopezj\Searchable\SearchableTrait;

class Product extends Model
{
    use SearchableTrait;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'products.name' => 10,
            'products.description' => 1,
        ],
    ];

    /**
     * Add or remove the product from favorites.
     */
    public function toggleLike()
    {
        $likes = session('likes', []);

        if (array_key_exists($this->id, $likes)) {
            unset($likes[$this->id]);
        } else {
            $likes[$this->id] = now();
        }

        session(compact('likes'));
    }

    /**
     * Check if the product is among favorites.
     *
     * @return bool
     */
    public function wasLiked()
    {
        return array_key_exists($this->id, session('likes', []));
    }

    /**
     * @inheritDoc
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
