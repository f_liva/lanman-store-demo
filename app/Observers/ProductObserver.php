<?php

namespace App\Observers;

use App\Models\Product;
use Illuminate\Support\Str;

class ProductObserver
{
    /**
     * Handle the Product "created" event.
     *
     * @param \App\Models\Product $product
     * @return void
     */
    public function saving(Product $product)
    {
        if (empty($product->slug)) {
            $product->slug = Str::slug($product->name);
        }
    }
}
