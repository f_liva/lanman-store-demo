[![LinkedIn][linkedin-shield]][linkedin-url]

## Getting Started

The steps necessary for the project to run smoothly! 🍦

1. Run `composer install`
2. Run `npm install && npm run prod`
3. Run `cp .env.example .env` and set `DB_DATABASE`, `DB_USER`, `DB_PASSWORD` and `APP_URL`
4. Run ` php artisan key:generate` to autofill `APP_KEY`
5. Run `php artisan migrate:fresh --seed` to seed the database.
6. Run `php artisan storage:link` to make product images accessible.
7. Run `php artisan nova:user` to configure your administrative user.

## Nice to know

* Laraval Nova was installed as a standalone to avoid to provide authorization to require laravel/nova repository as Composer dependency.

## Hire me

I've tried to include a variety of both frontend and backend functionality to show you my approach to development on both sides.

I hope to be able to demonstrate my full potential in the not too distant future.

Thanks for the opportunity. 🙏

[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/federicolivadev
